#include <strings.h>

void *memcpy(void *d,
             const void *s,
             int count) {
    char *dest = (char *)d;
    char *src = (char *)s;

    for (int i = 0; i < count; i++) {
        dest[i] = src[i];
    }
    return dest;
}

void *memcpy2(void *d,
              const void *s,
              int length,
              int count) {
    char *dest = (char *)d;
    char *src = (char *)s;

    for (int i = 0; i < count; i++) {
        dest[i] = src[i%length];
    }
    return dest;
}


void *memset(void *d,
             unsigned char val,
             int count) {
    char *dest = (char *)d;

    for (int i = 0; i < count; i++) {
        dest[i] = val;
    }
    return dest;
}

u32int memcmp(void *m1, void *m2, u32int l){
    char *mem1 = (char *)m1;
    char *mem2 = (char *)m2;

    u32int count = 0;
    for (u32int i = 0; i < l; i++) {
        if (mem1[i] != mem2[i]) {
            count++;
        }
    }

    return count;

}

int itoa(void *buf, int base, int d) {
    char *p = buf;
    char *p1, *p2;
    unsigned long ud = d;
    int divisor = 10;

    if (base == 'd' && d < 0) {
        *p++ = '-';
        buf++;
        ud = -d;
    } else if (base == 'x') {
        divisor = 16;
    }

    u32int t = 0;

    do {
        int remainder = ud % divisor;
        *p++ = (remainder < 10) ? remainder + '0' : remainder + 'a' - 10;
        t++;
    } while (ud /= divisor);
    
    for (u32int i = t; i < 2; i++) {
        *p++ = '0';
    }

    *p = 0;
    p1 = buf;
    p2 = p - 1;

    while (p1 < p2) {
        char tmp = *p1;
        *p1 = *p2;
        *p2 = tmp;
        p1++;
        p2--;
    }
    return 0;
}

void dump(void *a, u32int size) {
    char *addr = (char *)a;
    u32int lines = size / 16;
    u32int offset = size % 16;
    
    for (u32int line = 0; line < lines; line++) {
        printf ("0x%x: ", addr + line * 16);
        for (u32int i = 0; i < 16; i++) {
            u8int c = addr[16*line+i];
            printf ("%x ", c);
        }

        printf("| ");
        for (u32int i = 0; i < 16; i++) {
            u8int c = addr[16*line+i];
            if (c >= 32) {
                printf ("%c", c);
            } else {
                printf (".");
            }
        }

        printf("\n");

    }

    if (offset > 0 ) {
        printf("0x%x: ", addr + (lines + 1)*16);
        for (u32int i = 0; i < offset; i++) {
            u8int c = addr[16*(lines+1)+i];
            printf ("%x ", c);
        }

        for (u32int i = 0; i < 16 - offset; i++) {
            printf ("   ");
        }

        printf("| ");
        for (u32int i = 0; i < offset; i++) {
            u8int c = addr[16*(lines+1)+i];
            if (c >= 32) {
                printf ("%c", addr[16*(lines+1)+i]);
            } else {
                printf (".");
            }
        }

        printf("\n");

    }
}


