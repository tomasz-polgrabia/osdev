global read_ata_hd
global reset_ata_hd
global check_ata_hd

COMMAND_REG equ 1f7h
STATUS_REG  equ 1f7h

check_ata_hd:
    push ebp
    mov  ebp, esp
    push edx

    mov dx, STATUS_REG
    xor eax, eax
    in  al, dx

    pop edx
    pop ebp
    ret

read_ata_hd:
    push ebp
    mov ebp, esp

    push eax
    push ebx
    push ecx
    push edx
    push edi

    mov ebx, [ebp+8]         ; the chs values
    mov ecx, [ebp+12]        ; the 16-bit number of sectors to read max 65536
    mov edi, [ebp+16]        ; the address of buffer to put data obtained

    mov edx, 1f6h            ; port to send drive & head numbers
    mov al, bh               ; head index in bh
    and al, 00001111b        ; head is only 4 bit long
    or  al, 10100000b        ; default 1010b in high nibble
    out dx, al

    mov edx, 1f2h            ; sector count port
    mov al,  cl              ; read ch sectors
    out dx,  al

    mov edx, 1f3h            ; sector number port
    mov al,  bl              ; bl is a sector index
    out dx,  al

    mov edx, 1f4h            ; cylinder low port
    mov eax, ebx             ; byte 2 in ebx, just above bh
    mov cl,  16
    shr eax, cl              ; shift down to al
    out dx, al

    mov edx, 1f5h            ; cylinder high port
    mov eax, ebx             ; byte 3 in ebx, just above byte 2
    mov cl,  24
    shr eax, cl              ; shift down to al
    out dx, al

    mov edx, 1f7h            ; command port
    mov al,  20h             ; read with retry
    out dx,  al

    .still_going:
    in al, dx
    test al, 8               ; the sector buffer requires servicing
    jz .still_going          ; until the sector buffer is ready

    mov eax, 256             ; to read 512/2 = 256 words = 1 sector
    xor bx, bx
    mov bl, ch               ; read ch sectors
    mul bx
    mov ecx, eax             ; ecx is a counter for insw
    mov edx, 1f8h            ; data port, in and out
    rep insw                 ; in to [rdi]

    pop edi
    pop edx
    pop ecx
    pop ebx
    pop eax

    pop ebp
    ret

wait_ready:
    mov edx, 17fh             ; status/command register
    xor eax, eax
.poll_status:
    in al, dx
    test al, 64               ; ready flag
    jz   .poll_status         ; waiting for ready status

    ret

reset_ata_hd:
    push ebp
    mov ebp, esp

    cli

    mov edx, 17fh
    mov al,  0ech             ; identify command
    out dx, al

    call wait_ready

    xor eax, eax
    in  al,  dx

    mov edx, 1f7h            ; command port
    xor eax, eax
    in  al, dx

    mov edx, 1f6h            ; port to send drive & head numbers
    mov al, 0
    out dx, al

    call wait_ready

    mov edx, 1f7h            ; command port
    mov al,  08h
    out dx,  al

    xor eax, eax
    in  al, dx

    xor eax, eax
    in  al, dx

    sti

    pop ebp
    ret
