section .text

global loadPageDirectory
global set_cr0
global get_cr0
global set_cr3
global get_cr3

loadPageDirectory:

push ebp
mov ebp, esp
mov eax, [esp+8]
mov cr3, eax
mov esp, ebp
pop ebp
ret

global enablePaging
enablePaging:
push ebp
mov ebp, esp
mov eax, cr0
or eax, 0x80000000
mov cr0, eax
mov esp, ebp
pop ebp
ret

global disablePaging2

disablePaging2:
push ebp
mov ebp, esp
mov eax, cr0
and eax, 0x7fffffff
pop ebp
ret

set_cr3:
    push ebp
    mov ebp, esp
    mov eax, [ebp+8]
    mov cr3, eax
    pop ebp
    ret

get_cr3:
    push ebp
    mov ebp, esp
    mov eax, cr3
    pop ebp
    ret

set_cr0:
    push ebp
    mov ebp, esp
    mov eax, [ebp+8]
    mov cr0, eax
    pop ebp
    ret

get_cr0:
    push ebp
    mov ebp, esp
    mov eax, cr0
    pop ebp
    ret
