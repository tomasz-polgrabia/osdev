global copy_page_physical

copy_page_physical:
    push ebp
    pushf

    push eax
    push ebx
    push ecx
    push edx

    cli ; disabling interruptions

    ; pobieramy adresy stron zanim wyłączymy stronnicowanie
    mov ebx, [esp+12]
    mov ecx, [esp+16]

    ; wyłączamy stronnicowanie
    mov edx, cr0
    and edx, 0x7fffffff
    mov cr0, edx
    ; wyłączono stronnicowanie

    mov edx, 1024

.loop:                         ; pętla kopiująca dane strony
    mov eax, [ebx]             ; kopiujemy dane
    mov [ecx], eax
    add ebx, 4                 ; przesuwamy wskaźniki
    add ecx, 4
    dec edx                    ; zmniejszamy licznik bajtów do przekopiowania
    cmp edx, 0 ; tu w kodzie tego nie było TODO potestuj
    jnz .loop

    ; przywracamy stronnicowanie
    mov edx, cr0
    or edx, 0x80000000
    mov cr0, edx
    ; przywrócono stronnicowanie


    sti ; enabling interruptions

    pop edx
    pop ecx
    pop ebx
    pop eax

    popf
    pop ebp
    ret
