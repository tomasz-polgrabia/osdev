#include <fs/initrd.h>

char *initrd = NULL;
char *initrd_data = NULL;
initrd_header_t *headers = NULL;
fs_node_t *initrd_root = NULL;
fs_node_t *initrd_dev = NULL;
fs_node_t *root_nodes = NULL;
u32int nroot_nodes = 0;
dirent_t dirent;

u32int strcmp(void *s1, void *s2) {
    char *m1 = (char *)s1;
    char *m2 = (char *)s2;
    u32int differences = 0;

    while (*m1 && *m2) {
        if (*m1 != *m2) {
            differences++;
        }
        ++m1;
        ++m2;
    }

    if (*m1 || *m2) {
        return differences + 1;
    }

    return differences;

}

void *strcpy(void *d, void *s) {
    void *copy = d;
    char *dest = (char *)d;
    char *src = (char *)s;
    while (*src) {
        *dest = *src;
        src++;
        dest++;
    }
    return copy;
}

fs_node_t *initrd_init(u32int addr) {
    initrd = (char *)addr;
    nroot_nodes = *((u32int *)(initrd));
    headers = (initrd_header_t *)(initrd + sizeof(char)*16);
    initrd_data = (char *)headers + sizeof(initrd_header_t) * nroot_nodes;

    initrd_root = (fs_node_t *)kmalloc(sizeof(fs_node_t));
    strcpy(initrd_root->name, "initrd");
    initrd_root->mask = initrd_root->uid = initrd_root->gid = 0;
    initrd_root->inode = initrd_root->length = 0;
    initrd_root->flags = FS_DIRECTORY;
    initrd_root->read = 0;
    initrd_root->write = 0;
    initrd_root->open = 0;
    initrd_root->close = 0;
    initrd_root->readdir = &readdir_initrd;
    initrd_root->finddir = &finddir_initrd;
    initrd_root->ptr = 0;
    initrd_root->impl = 0;

    initrd_dev = (fs_node_t*)kmalloc(sizeof(fs_node_t));
    strcpy(initrd_dev->name, "dev");
    initrd_dev->mask = initrd_dev->uid = initrd_dev->gid = initrd_dev->inode = initrd_dev->length = 0;
    initrd_dev->flags = FS_DIRECTORY;
    initrd_dev->read = 0;
    initrd_dev->write = 0;
    initrd_dev->open = 0;
    initrd_dev->close = 0;
    initrd_dev->readdir = &readdir_initrd;
    initrd_dev->finddir = &finddir_initrd;
    initrd_dev->ptr = 0;
    initrd_dev->impl = 0;

    root_nodes = (fs_node_t*)kmalloc(sizeof(fs_node_t) * nroot_nodes);

    for (u32int i = 0; i < nroot_nodes; i++) {
        strcpy(root_nodes[i].name, &headers[i].name);
        root_nodes[i].mask = root_nodes[i].uid = root_nodes[i].gid = 0;
        root_nodes[i].length = headers[i].length;
        root_nodes[i].inode = i;
        root_nodes[i].flags = FS_FILE;
        root_nodes[i].read = &read_initrd;
        root_nodes[i].write = 0;
        root_nodes[i].readdir = 0;
        root_nodes[i].finddir = 0;
        root_nodes[i].open = 0;
        root_nodes[i].close = 0;
        root_nodes[i].impl = 0;
    }

    return initrd_root;

}

u32int read_initrd(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer) {
    initrd_header_t header = headers[node->inode];

    if (offset >= header.length) {
        return 0;
    }

    if (offset + size > header.length) {
        size = header.length - offset;
    }

    memcpy(buffer, initrd_data + header.offset, size);

    return size;
}

u32int write_initrd(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer) {
    UNUSED(node); UNUSED(offset); UNUSED(size);
    UNUSED(buffer);

    // system plików read-only

    return 0;
}

void open_initrd(fs_node_t *node,
             u8int read,
             u8int write) {
    UNUSED(node); UNUSED(read); UNUSED(write);

    // system plików in-memory, nic nie robimy
}

void close_initrd(fs_node_t *node) {
    UNUSED(node);
    // system plików in-memory, nic nie robimy
}

struct dirent *readdir_initrd(fs_node_t *node,
        u32int index) {
    if (node == initrd_root && index == 0) {
        memcpy(dirent.name, "dev", 3);
        dirent.name[3] = 0;
        dirent.ino = 0;
        return &dirent;
    }

    if (index - 1 >= nroot_nodes) {
        return NULL;
    }

    memcpy(dirent.name, root_nodes[index-1].name, INITRD_NAME_LENGTH);
    dirent.ino = root_nodes[index-1].inode;

    return &dirent;
}

fs_node_t *finddir_initrd(fs_node_t *node,
                      char *name) {

    if (node == initrd_root && memcmp(name, "dev", 3) == 0) {
        return initrd_dev;
    }

    for (u32int i = 0; i < nroot_nodes; i++) {
        if (strcmp(name, root_nodes[i].name) == 0) {
            return &root_nodes[i];
        }
    }

    return NULL;
}

