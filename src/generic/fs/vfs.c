#include <fs/vfs.h>

u32int read_fs(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer) {
    if (node->read != NULL) {
        return node->read(node,
                          offset,
                          size,
                          buffer);
    } else {
        return NULL;
    }
}

u32int write_fs(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer) {
    if (node->write != NULL) {
        return node->write(node,
                           offset,
                           size,
                           buffer);
    } else {
        return NULL;
    }
}

void open_fs(fs_node_t *node,
             u8int read,
             u8int write) {
    UNUSED(read); UNUSED(write);
    if (node->open != NULL) {
        node->open(node);
    }
}

void close_fs(fs_node_t *node) {
    if (node->close != NULL) {
        node->close(node);
    }
}

struct dirent *readdir_fs(fs_node_t *node,
        u32int index) {
    if (node->readdir != NULL) {
        return node->readdir(node,
                             index);
    } else {
        return NULL;
    }
}

fs_node_t *finddir_fs(fs_node_t *node,
                      char *name) {
    if (node->finddir != NULL) {
        return node->finddir(node,
                             name);
    } else {
        return NULL;
    }
}

