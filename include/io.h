#ifndef __IO_H__
#define __IO_H__

#include <types.h>

extern void outb(u16int port, u8int data);

extern u8int inb(u16int  port);

extern void outw(u16int port, u16int data);

extern u16int inw(u16int  port);

#endif
