#ifndef __FRAME_H__
#define __FRAME_H__

#include <basic.h>
#include <types.h>
#include <generic.h>
#include <page.h>

void set_frame(u32int frame_addr);
void clear_frame(u32int frame_addr);
u32int test_frame(u32int frame_addr);
u32int first_frame();
void alloc_frame(page_t *p, int is_kernel, int is_writable);
void ident_alloc_frame(page_t *p, int is_kernel, int is_writable, u32int addr);
void free_frame(page_t *p);
u32int frames_allocated();
void dump_frames();

#endif
