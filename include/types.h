#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned int u32int;
typedef unsigned short int u16int;
typedef unsigned char u8int;
typedef unsigned long long int u64int;

#endif
