#ifndef __FS_INITRD_H__
#define __FS_INITRD_H__

#include <types.h>
#include <fs/vfs.h>
#include <heap.h>
#define INITRD_NAME_LENGTH 128
#define INITRD_MAGIC 0xdeadbeaf


typedef struct initrd_header {
    u32int magic;
    char   name[INITRD_NAME_LENGTH];
    u32int offset;
    u32int length;
} __attribute__ ((aligned (16))) initrd_header_t;

fs_node_t *initrd_init(u32int addr);

u32int read_initrd(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer);

u32int write_initrd(fs_node_t *node,
               u32int offset,
               u32int size,
               u8int *buffer);

void open_initrd(fs_node_t *node,
             u8int read,
             u8int write);

void close_initrd(fs_node_t *node);

struct dirent *readdir_initrd(fs_node_t *node,
        u32int index);

fs_node_t *finddir_initrd(fs_node_t *node,
                      char *name);


#endif
