#ifndef __TASK_COPY__
#define __TASK_COPY__

#include <page.h>
#include <heap.h>
#include <task/process.h>

page_directory_t *clone_directory(page_directory_t *src);
page_table_t *clone_table(page_table_t *src, u32int *phys);

#endif
