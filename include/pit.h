#ifndef __PIT_H__
#define __PIT_H__

#include <io.h>
#include <console.h>
#include <irq.h>
#include <generic.h>

int timer_phase(int hz);
int timer_handler(struct regs *r);
int timer_install();
int sleep(double sec);
#endif
