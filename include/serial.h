#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <io.h>
#include <console.h>

#define PORT 0x3F8

void init_serial();
int serial_received();
char read_serial();
int is_transmit_empty();
void write_serial(char a);
void write_serial_string(char *s);

#endif
