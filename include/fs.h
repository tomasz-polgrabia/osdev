#ifndef __FS_H__
#define __FS_H__

void read_ata_hd(unsigned int chs, unsigned int nr_sectors, unsigned char *buf);
void reset_ata_hd();
int  check_ata_hd();

#endif
