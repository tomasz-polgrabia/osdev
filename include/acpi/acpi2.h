#ifndef  __ACPI2__H
#define  __ACPI2__H

#include <generic.h>
#include <io.h>
#include <console.h>
#include <pit.h>

typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned int   dword;

struct RSDPtr {
    byte Signature[8];
    byte CheckSum;
    byte OemID[6];
    byte Revision;
    dword *RsdtAddress;
};

struct FACP {
    byte Signature[4];
    dword Length;
    byte unneeded1[40 - 0];
    dword *DSDT;
    byte unneeded2[48 - 44];
    dword *SMI_CMD;
    byte ACPI_ENABLE;
    byte ACPI_DISABLE;
    byte unneeded3[64 - 54];
    dword *PM1a_CNT_BLK;
    dword *PM1b_CNT_BLK;
    byte unneeded4[89 - 72];
    byte PM1_CNT_LEN;
};

int initAcpi(void);
int acpiEnable(void);
void acpiPowerOff();

#endif
