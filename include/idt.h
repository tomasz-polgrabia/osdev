#ifndef __IDT_H__
#define __IDT_H__

#include <strings.h>

struct idt_entry {
    unsigned short base_lo;
    unsigned short sel;
    unsigned char always0;
    unsigned char flags;
    unsigned short base_hi;
} __attribute__((packed));

struct idt_ptr {
    unsigned short limit;
    unsigned int base;
} __attribute__((packed));

extern int idt_load();

int idt_set_gate(unsigned char num,
                 unsigned long base,
                 unsigned short sel,
                 unsigned char flags);

int idt_install();

#endif
