#ifndef __MEM_H__
#define __MEM_H__

#include <multiboot.h>
#include <generic.h>
#include <types.h>

extern u32int end;
extern void disablePaging();
extern multiboot_info_t *m;
extern u32int mem_lower;
extern u32int mem_upper;

#endif
