#ifndef __KBD_H__
#define __KBD_H__

#include <io.h>
#include <isr.h>
#include <irq.h>
#include <console.h>
#include <generic.h>

extern unsigned char kbdus[128];

int keyboard_handler(struct regs *r);
int kbd_install();

#endif
