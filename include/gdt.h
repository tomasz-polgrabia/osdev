#ifndef __GDT_H__
#define __GDT_H__

struct gdt_entry {

    unsigned short limit_low;
    unsigned short base_low;
    unsigned char base_middle;
    unsigned char access;
    unsigned char granularity;
    unsigned char base_hight;

} __attribute__ ((packed));

struct gdt_ptr {
    unsigned short limit;
    unsigned int base;
} __attribute__ ((packed));

extern int gdt_flush();
extern struct gdt_entry gdt[3];
extern struct gdt_ptr gp;

int gdt_set_gate(int num,
                 unsigned long base,
                 unsigned long limit,
                 unsigned char access,
                 unsigned char gran);

int gdt_install();


#endif
