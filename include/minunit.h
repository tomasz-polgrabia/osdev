/* file: minunit.h */
#define INTERNAL_ERROR "INTERNAL ERROR. CHECK FOR BUFFER LENGTH."
#define TRUNCATED_ERROR "RESULT TRUNCATED. INCREASE TEMP BUFFER LIMIT"

#define mu_assert(message, test) do { if (!(test)) { \
        if (snprintf(temp, temp_limit, "[%s(%s):%d]: %s\n", __FILE__, \
                    __FUNCTION__, __LINE__, message) >= temp_limit) { \
            return TRUNCATED_ERROR; \
        } \
        return(temp); \
    }} while (0)

#define mu_run_test(test) do { char *message = test(); tests_run++; \
                                if (message) { \
                                        if(!handle(message)) { \
                                            return INTERNAL_ERROR; \
                                        } \
                                    } \
                                    else tests_succeeded++; } while (0)

 extern char *temp;
 extern int temp_limit;
 extern int mu_sprintf(char *buffer, int size, const char *fmt, ...);
 extern unsigned int limit;
 extern char *buffer;
 extern unsigned int pos;
 extern int tests_run;
 extern int tests_succeeded;

 extern int mu_strlen(const char *);
 extern void *mu_memcpy(void *, const void*, unsigned int);

 int handle(char *msg) {
    unsigned int n = (unsigned int)mu_strlen(msg);
    if (pos >= limit) {
        return 0;
    }

    if (limit-pos < n) {
        return 0;
    }

    mu_memcpy(buffer+pos, msg, n);
    pos += n;

    return 1;
 }
